/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructure.st1300;

import java.util.Arrays;
import mx.edu.utr.datastructures.*;

/**
 *
 * @author tomye
 */
public class ArrayList implements List {
private Object[] elements;
    private int size;
    
    public ArrayList(){
        this(10);
    }
 /**
 *
 * {@code 
     * @param initialCapacity
 */
    public ArrayList(int initialCapacity){
        elements= new Object[initialCapacity];
    }
     
/**
* Appends the specified element to the end of this list.
     * @param element
*/
    @Override
    public boolean add(Object element){
        ensureCapacity(size + 1);
        elements [size++] = element;
        return true;
    }
    
/**
* Returns the number of elements in this list.
*/
    @Override
    public int size(){
        return size;
    }
    
/**
* Returns <tt>true</tt> if this list contains no elements.
*/
    @Override
    public boolean isEmpty(){
        return size==0; //es igual a lo siguiente
//        if (size == 0 ){
//            return true;
//        }else{
//            return false;
//        }
    }
    
/**
* Returns the element at the specified position in this list.    
*/    
    @Override
    public Object get(int index) {
     if(index >= size){
         throw new IndexOutOfBoundsException();
     }     
      return elements[index];     
    }
    
/**
* Replaces the element at the specified position in this list with the
* specified element.  
*/
    @Override
    public Object set(int index, Object element){
           rangeCheckAdd(index);
            Object old= elements[index];
            elements[index]= element;
            return old;
        
    }
    
    


    private void rangeCheckAdd(int index){
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }
    
    /*private void outOfBound(int index){
        if (index >= size){
            throw new ArrayIndexOutOfBoundsException("Out of Bound");
        }
    }*/
    
/**
* Inserts the specified element at the specified position in this list.
*/
    @Override
    public void add(int index, Object element) {
        checkRangeAdd(index);
        ensureCapacity(size + 1);
        for (int i = size - 1; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }
    
/**
* Returns the index in this list of the first occurrence of the specified
* element, or -1 if this list does not contain this element.
     * @param element
*/
    @Override
    public int indexOf(Object element) {
        if (element == null){
            for(int i=0; i<size; i++){
                if(element.equals(elements[i])){
                    return i;
                }
            }
        }else{
            for(int i=0; i<size; i++){
                if(element.equals(elements[i])){
                    return i;
                }
            }
        }
        return -1;
    }

    
 /**
 *removes the element of an specific position of the list.
 */
    @Override
    public Object remove(int index) {
        rangeCheckAdd(index);
        Object oldElement = elements[index];
        int elementMoved = size - index -1;
        if(elementMoved > 0){
            System.arraycopy(elements, index + 1, elements, index, elementMoved);
        }
        elements[--size] = null;
        return oldElement;
    }

 
    private Exception Exception(String out_bound) {
        throw new IndexOutOfBoundsException(); //To change body of generated methods, choose Tools | Templates.
    }

/**
* Removes all of the elements from this list. *
*/
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
            size = 0;
        }
    }
    private void checkRange(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Invalid Index");
        }
    }

    private void checkRangeAdd(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Invalid Index");
        }
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            Object[] oldData = elements;
            elements = new Object[newCapacity];
            for (int i = 0; i < size; i++) {
                elements[i] = oldData[i];
                //System.arraycopy(oldData, 0, elements, 0, size);
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }
    
}




/*private void checkRange(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Invalid Index");
        }
    }

    private void checkRangeAdd(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Invalid Index");
        }
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            Object[] oldData = elements;
            elements = new Object[newCapacity];
            for (int i = 0; i < size; i++) {
                elements[i] = oldData[i];
                //System.arraycopy(oldData, 0, elements, 0, size);
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }*/